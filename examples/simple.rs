extern crate blockweb;

use blockweb::{Block, Blockweb, LocalServer, MemoryStorage, Storage};
use std::sync::Arc;
use std::thread;

fn main() {
    let memory_storage = Arc::new(MemoryStorage::new());

    memory_storage.add(Block::new_genisis());

    let local_server = LocalServer::new(memory_storage);
    let blockweb = Blockweb::new(local_server);

    let mut handles = Vec::new();

    const THREADS: usize = 4;
    const ITEMS: usize = 10;

    for thread_id in 0..THREADS {
        let b = blockweb.clone();

        let handle = thread::spawn(move || {
            for i in 0..ITEMS {
                println!("{} processing item {} of {}", thread_id, i, ITEMS);
                b.add(i);
            }
        });

        handles.push(handle);
    }

    for handle in handles {
        match handle.join() {
            Ok(_) => {}
            Err(error) => println!("{:?}", error),
        }
    }

    let leaves = blockweb.server().storage().leaves();
    let blocks_len = blockweb.server().storage().blocks().len();

    println!("{:?} {:?}", leaves.len(), blocks_len);
}
