use serde::Serialize;

use super::super::{Block, Id};

pub trait Storage<T>
where
    T: Serialize,
{
    fn get(&self, &Id) -> Option<Block<T>>;
    fn add(&self, Block<T>);
    fn leaves(&self) -> Vec<Block<T>>;
}
