use std::collections::HashMap;
use std::sync::{Arc, RwLock, RwLockReadGuard, RwLockWriteGuard};

use serde::Serialize;

use super::super::{Block, Id};
use super::Storage;

#[derive(Debug, Clone)]
pub struct MemoryStorage<T>
where
    T: Serialize,
{
    blocks: Arc<RwLock<HashMap<Id, Block<T>>>>,
}

impl<T> MemoryStorage<T>
where
    T: Serialize,
{
    #[inline]
    pub fn new() -> Self {
        MemoryStorage {
            blocks: Arc::new(RwLock::new(HashMap::default())),
        }
    }

    #[inline]
    pub fn blocks(&self) -> RwLockReadGuard<HashMap<Id, Block<T>>> {
        self.blocks
            .read()
            .expect("failed to aquire blocks read lock")
    }

    #[inline]
    pub fn blocks_mut(&self) -> RwLockWriteGuard<HashMap<Id, Block<T>>> {
        self.blocks
            .write()
            .expect("failed to aquire blocks write lock")
    }
}

impl<T> Storage<T> for MemoryStorage<T>
where
    T: Clone + Serialize,
{
    #[inline]
    fn get(&self, id: &Id) -> Option<Block<T>> {
        self.blocks().get(id).map(Clone::clone)
    }
    #[inline]
    fn add(&self, block: Block<T>) {
        self.blocks_mut().insert(block.id().clone(), block);
    }
    #[inline]
    fn leaves(&self) -> Vec<Block<T>> {
        let mut leaves = Vec::new();

        let blocks = self.blocks();

        for (id, block) in blocks.iter() {
            let mut is_leaf = true;

            for (_, other) in blocks.iter() {
                if id == other.left_id() || id == block.right_id() {
                    is_leaf = false;
                }
            }

            if is_leaf {
                leaves.push(block.clone());
            }
        }

        leaves
    }
}
