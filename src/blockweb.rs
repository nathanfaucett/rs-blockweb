use std::marker::PhantomData;

use serde::Serialize;

use super::{Block, Id, Server};

#[derive(Debug, Clone)]
pub struct Blockweb<T, S>
where
    T: Serialize,
    S: Server<T>,
{
    server: S,
    _phantom_data: PhantomData<(T, S)>,
}

impl<T, S> Blockweb<T, S>
where
    T: Serialize,
    S: Server<T>,
{
    #[inline(always)]
    pub fn new(server: S) -> Self {
        Blockweb {
            server: server,
            _phantom_data: PhantomData,
        }
    }

    #[inline(always)]
    pub fn server(&self) -> &S {
        &self.server
    }

    #[inline]
    fn validate_block(&self, block: &Block<T>) -> Id {
        let left_id = if let Some(left) = self.server.get(block.left_id()) {
            self.validate_block(&left)
        } else {
            Id::new()
        };
        let right_id = if let Some(right) = self.server.get(block.right_id()) {
            self.validate_block(&right)
        } else {
            Id::new()
        };

        block.validate(&left_id, &right_id)
    }

    #[inline]
    fn process(&self, value: Option<T>) -> &Self {
        let (left_id, right_id) = self.server.pull();
        let weight = left_id.weight() + right_id.weight();

        let block = Block::new(left_id.id().clone(), right_id.id().clone(), weight, value);

        assert_eq!(
            block.id(),
            &self.validate_block(&block),
            "failed to validate block"
        );
        self.server.push(block);

        self
    }

    #[inline(always)]
    pub fn add(&self, value: T) -> &Self {
        self.process(Some(value))
    }

    #[inline(always)]
    pub fn add_empty(&self) -> &Self {
        self.process(None)
    }
}
