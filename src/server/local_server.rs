use std::marker::PhantomData;
use std::sync::Arc;

use rand;
use serde::Serialize;

use super::super::{Block, Id, Storage};
use super::Server;

#[derive(Debug, Clone)]
pub struct LocalServer<T, S>
where
    T: Serialize,
    S: Storage<T>,
{
    storage: Arc<S>,
    _phatom_data: PhantomData<(T, S)>,
}

impl<T, S> LocalServer<T, S>
where
    T: Serialize,
    S: Storage<T>,
{
    #[inline]
    pub fn new(storage: Arc<S>) -> Self {
        LocalServer {
            storage: storage,
            _phatom_data: PhantomData,
        }
    }

    #[inline]
    pub fn storage(&self) -> &S {
        &self.storage
    }
}

impl<T, S> Server<T> for LocalServer<T, S>
where
    T: Clone + Serialize,
    S: Storage<T>,
{
    #[inline(always)]
    fn get(&self, id: &Id) -> Option<Block<T>> {
        self.storage.get(id)
    }
    #[inline(always)]
    fn push(&self, block: Block<T>) {
        self.storage.add(block);
    }
    #[inline]
    fn pull(&self) -> (Block<T>, Block<T>) {
        let mut leaves = self.storage.leaves();

        if leaves.len() == 1 {
            let empty = {
                let leaf = &leaves[0];

                Block::new(
                    leaf.id().clone(),
                    leaf.id().clone(),
                    leaf.weight().clone(),
                    None,
                )
            };

            self.storage.add(empty.clone());

            (leaves.remove(0), empty)
        } else {
            leaves.sort_by(|a, b| a.weight().cmp(b.weight()));

            println!(
                "{:?}",
                leaves
                    .iter()
                    .map(|b| b.weight().to_str_radix(10))
                    .collect::<Vec<String>>()
            );

            if leaves.len() > 3 {
                let max = leaves.len() as f32 * 0.5;
                let i0 = (rand::random::<f32>() * max) as usize;
                let i1 = {
                    let mut index = (rand::random::<f32>() * max) as usize;

                    while index == i0 {
                        index = (rand::random::<f32>() * max) as usize;
                    }

                    index
                };

                (leaves.remove(i0), leaves.remove(i1))
            } else {
                (leaves.remove(0), leaves.remove(0))
            }
        }
    }
}
