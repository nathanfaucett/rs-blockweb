use serde::Serialize;

use super::super::{Block, Id};

pub trait Server<T>
where
    T: Serialize,
{
    fn get(&self, &Id) -> Option<Block<T>>;
    fn push(&self, Block<T>);
    fn pull(&self) -> (Block<T>, Block<T>);
}
