use chrono::{DateTime, Utc};
use num::{BigUint, FromPrimitive};

#[inline]
pub fn timestamp() -> BigUint {
    let utc: DateTime<Utc> = Utc::now();
    let nanos = utc.timestamp_subsec_nanos() as u64;
    let timestamp = ((utc.timestamp() as u64) * 1_000_000_000) + nanos;
    BigUint::from_u64(timestamp).unwrap()
}
