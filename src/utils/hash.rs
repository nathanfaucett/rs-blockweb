use sha2::{Digest, Sha512};

#[inline]
pub fn hash(bytes: &[u8]) -> Vec<u8> {
    let mut hasher = Sha512::default();
    hasher.input(bytes);
    hasher.result().to_vec()
}
