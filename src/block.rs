use std::fmt;

use bincode;
use num::{BigUint, One};
use serde::Serialize;

use super::{utils, Id};

#[derive(Clone, Serialize)]
pub struct Block<T>
where
    T: Serialize,
{
    id: Id,
    left_id: Id,
    right_id: Id,
    weight: BigUint,
    timestamp: BigUint,
    value: Option<T>,
}

impl<T> fmt::Debug for Block<T>
where
    T: fmt::Debug + Serialize,
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_struct("Block")
            .field("id", &self.id)
            .field("left_id", &self.left_id)
            .field("right_id", &self.right_id)
            .field("weight", &self.weight.to_str_radix(10))
            .field("timestamp", &self.timestamp.to_str_radix(10))
            .field("value", &self.value)
            .finish()
    }
}

impl<T> Block<T>
where
    T: Serialize,
{
    #[inline]
    pub fn new(left_id: Id, right_id: Id, weight: BigUint, value: Option<T>) -> Self {
        let timestamp = utils::timestamp();

        let value_bytes = bincode::serialize(&value).unwrap();
        let weight_bytes = bincode::serialize(&weight).unwrap();
        let timestamp_bytes = bincode::serialize(&timestamp).unwrap();

        let mut id_bytes = Vec::with_capacity(
            left_id.len()
                + right_id.len()
                + weight_bytes.len()
                + timestamp_bytes.len()
                + value_bytes.len(),
        );

        id_bytes.extend(left_id.as_slice());
        id_bytes.extend(right_id.as_slice());
        id_bytes.extend(weight_bytes);
        id_bytes.extend(timestamp_bytes);
        id_bytes.extend(value_bytes);

        Block {
            id: utils::hash(&id_bytes).into(),
            left_id: left_id,
            right_id: right_id,
            weight: weight,
            timestamp: timestamp,
            value: value,
        }
    }

    #[inline]
    pub fn new_genisis() -> Self {
        Self::new(Id::new(), Id::new(), BigUint::one(), None)
    }

    #[inline(always)]
    pub fn id(&self) -> &Id {
        &self.id
    }
    #[inline(always)]
    pub fn left_id(&self) -> &Id {
        &self.left_id
    }
    #[inline(always)]
    pub fn right_id(&self) -> &Id {
        &self.right_id
    }
    #[inline(always)]
    pub fn weight(&self) -> &BigUint {
        &self.weight
    }
    #[inline(always)]
    pub fn timestamp(&self) -> &BigUint {
        &self.timestamp
    }
    #[inline]
    pub fn value(&self) -> Option<&T> {
        self.value.as_ref()
    }

    #[inline]
    pub fn validate(&self, left_id: &Id, right_id: &Id) -> Id {
        let value_bytes = bincode::serialize(&self.value).unwrap();
        let weight_bytes = bincode::serialize(&self.weight).unwrap();
        let timestamp_bytes = bincode::serialize(&self.timestamp).unwrap();

        let mut id_bytes = Vec::with_capacity(
            left_id.len()
                + right_id.len()
                + weight_bytes.len()
                + timestamp_bytes.len()
                + value_bytes.len(),
        );

        id_bytes.extend(left_id.as_slice());
        id_bytes.extend(right_id.as_slice());
        id_bytes.extend(weight_bytes);
        id_bytes.extend(timestamp_bytes);
        id_bytes.extend(value_bytes);

        utils::hash(&id_bytes).into()
    }
}
