use std::fmt;
use std::ops::Deref;

use base64;

#[derive(Clone, Serialize, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct Id(Vec<u8>);

impl From<Vec<u8>> for Id {
    #[inline(always)]
    fn from(vec: Vec<u8>) -> Self {
        Id(vec)
    }
}

impl Deref for Id {
    type Target = Vec<u8>;

    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl Id {
    #[inline(always)]
    pub fn new() -> Self {
        Id(Vec::new())
    }
}

impl fmt::Debug for Id {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_tuple("Id").field(&base64::encode(&self.0)).finish()
    }
}
